import React, {Component} from 'react';

export default class CadProd extends Component {

    state = {
        error: null,
        isLoaded: false,
        lojas: [],
        produtos:[]
    };

    async componentDidMount() {  
        await this.getLojas(); 
        await this.getProdutos(); 
         
    }
    
    async getLojas() { 
        this.setState({isLoading: true});
        
        const response = await fetch( "http://localhost:8080/lojas");
        const lojas = await response.json();
        
        if(lojas.error) this.setState({ error: lojas.error, isLoading: false});
        await this.setStateAsync({ lojas })
        this.setState({ isLoading: false })
    }

    async getProdutos() { 
        this.setState({isLoading: true});
        
        const response = await fetch( "http://localhost:8080/produtos");
        const produtos = await response.json();
        if(produtos.error) this.setState({ error: produtos.error, isLoading: false});
        await this.setStateAsync({ produtos })
        this.setState({ isLoading: false })
    }

    setStateAsync(state) {
		return new Promise((resolve) => {
			this.setState(state, resolve)
		});
    }

    gravar = async (event) =>  {
        event.preventDefault();
        const codFilial     =  parseInt(this.state.codFilial.value,10); 
        const codigoproduto =  parseInt(this.state.codigoproduto.value,10);
        const descricao     = this.state.descricao.value;
        const preco         = this.state.preco.value;
    
        this.setState({isLoading: true});
        const myHeaders = new Headers(); myHeaders.append("Content-Type", "application/json");
        const config    = { headers:myHeaders,method:'POST',body:JSON.stringify({codFilial,codigoproduto,descricao,preco}) }
                            
        const response = await fetch("http://localhost:8080/produtos",config);
        const lojas    = await response.json();
        
        this.state.codigoproduto.value = '';
        this.state.descricao.value = '';
        this.state.preco.value = '';

        this.setState({ isLoading: false })
        this.getProdutos(); 
    }

     //método para gravação do produto.  
     deletar = async (event) => {
       // event.preventDefault();
        const codFilial     = event.codFilial;
        const codproduto    = event.codproduto;

        this.setState({isLoading: true});
        const myHeaders = new Headers(); 
        myHeaders.append("Content-Type", "application/json");        
        const config    = { headers:myHeaders,method: 'delete',body:JSON.stringify({codFilial,codproduto})}
                        
        const response  = await fetch("http://localhost:8080/produtos",config);
        const produtos  = await response.json();       
        this.setState({ isLoading: false })
        this.getProdutos();   
     }

    render() {      
        const { error, isLoading, lojas, produtos } = this.state;                
        return(
            <div className="container text-center">
                <div className="container col-12 col-sm-6">
                    <form>
                        <div className="form-group">
                        <label htmlFor="exampleSelect1">Loja</label>
                        <select className="form-control" id="exampleSelect1" ref={(option) => {if (option) this.state.codFilial = option;}}>
                        {lojas.map(loja => ( 
                            <option key = {loja.codFilial} value={loja.codFilial} >{loja.codFilial} - {loja.descricao}</option>
                        ))}  
                        </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="formGroupExampleInput2">Código do Produto</label>
                            <input type="text" className="form-control" id="formGroupExampleInput2" ref={(input) => {if (input) this.state.codigoproduto = input;  }}  placeholder="Código do Produto" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="formGroupExampleInput2">Descrição do Produto</label>
                            <input type="text" className="form-control" id="formGroupExampleInput2" ref={(input) => {if (input) this.state.descricao = input;  }}  placeholder="Descrição" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="formGroupExampleInput2">Preço do Produto</label>
                            <input type="text" className="form-control" id="formGroupExampleInput2" ref={(input) => {if (input) this.state.preco = input;  }}  placeholder="Preço" />
                        </div>
                            <button style={{float:'right',cursor:'pointer'}}title="Salvar Loja" type="button" onClick={(event)=>this.gravar(event)} className="btn btn-success">Salvar</button>
                    </form>
                </div> 
                <table className="table table-hover">
                            <thead>
                                <tr>
                                    <th>Loja</th>
                                    <th>Cód Produto</th>
                                    <th>Produto</th>                       
                                    <th>Valor do Produto</th>                       
                                </tr>
                            </thead>
                            <tbody>
                            {produtos.map(produto => (
                                <tr key = {produto.chaveKey}>                                         
                                    <td>{produto.codFilial}</td>                     
                                    <td>{produto.codproduto}</td>
                                    <td>{produto.descricao}</td>
                                    <td>{produto.vlrVendas}</td>                                   
                                    <td><button className="btn btn-danger"  style={{cursor:'pointer'}} onClick={(event)=>this.deletar(produto)}>Deletar</button></td>
                                </tr> 
                            ))}                   
                            </tbody>
                        </table>

                </div>
          
        );
    }
}

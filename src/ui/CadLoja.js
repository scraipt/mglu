import * as React from 'react';

export default class CadLoja extends React.Component {

  
    state = {
        error: null,
        isLoaded: false,
        lojas: [],
        codAlter: '',
        descAlter:'',
        cepAlter: ''
    };
     

    async componentDidMount() {  
    await this.getLojas(); 
     
    }
   
    async getLojas() { 
        this.setState({isLoading: true});
        
        const response = await fetch( "http://localhost:8080/lojas");
        const lojas = await response.json();
        
        if(lojas.error) this.setState({ error: lojas.error, isLoading: false});
        await this.setStateAsync({ lojas })
		this.setState({ isLoading: false })
    }
    
    setStateAsync(state) {
		return new Promise((resolve) => {
			this.setState(state, resolve)
		});
    }
    //método para gravação da loja  
     gravar = async (event) =>  {
        event.preventDefault();
        const codigo =  parseInt(this.state.codigo.value,10);
        const nomeloja = this.state.nomeloja.value;
        const ceploja = this.state.ceploja.value;
    
        this.setState({isLoading: true});
        const myHeaders = new Headers(); myHeaders.append("Content-Type", "application/json");
        const config = { headers:myHeaders,method:'POST',body:JSON.stringify({codFilial:codigo,descricao:nomeloja,cep:ceploja,}) }
                            
        const response = await fetch("http://localhost:8080/lojas",config);
        const lojas = await response.json();
        
        this.state.codigo.value = '';
        this.state.nomeloja.value = '';
        this.state.ceploja.value = '';

        this.setState({ isLoading: false })
        this.getLojas();
    }
    //método para gravação da loja  
    deletar = async (event) => {
       // event.preventDefault();
        const codigo = event.codFilial;
        
        this.setState({isLoading: true});
        const myHeaders = new Headers(); 
        myHeaders.append("Content-Type", "application/json");        
        const config = { headers:myHeaders,method: 'delete',body:JSON.stringify({codFilial:codigo}) }
                            
        const response = await fetch("http://localhost:8080/lojas",config);
        const lojas = await response.json();       
        this.setState({ isLoading: false })
        this.getLojas();      
    }


    render() {
        const { error, isLoading, lojas } = this.state;       
        return(
            <div>
                <div className="container">
                    <div className="col-lg-6 mx-auto">
                        <form>
                        <div className="form-group">
                            <label htmlFor="codigo">Código da Loja</label>
                            <input type="text" className="form-control" id="codigo" ref={(input) => {if (input) this.state.codigo = input;  }}  placeholder="Digite o código da empresa"/>                       
                        </div>
                        <div className="form-group">
                            <label htmlFor="nomeloja">Nome da Loja</label>
                            <input type="text" className="form-control" id="nomeloja"  ref={(input) => {if (input) this.state.nomeloja = input; }}  placeholder="Digite o nome da Loja"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="ceploja">Nome da Loja</label>
                            <input type="text" className="form-control" id="ceploja" ref={(input) => {if (input) this.state.ceploja = input;  }}  placeholder="Digite o CEP da Loja"/>
                        </div>
                        <button type="button" style={{float:'right',cursor:'pointer'}} className="btn btn-success" onClick={(event)=>this.gravar(event)}>Gravar</button>
                        </form>
                    </div>  
                        <table className="table table-hover">
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Nome</th>
                                    <th>CEP</th>                       
                                </tr>
                            </thead>
                            <tbody>
                            {lojas.map(loja => (
                                <tr key = {loja.codFilial}>                        
                                    <td>{loja.codFilial}</td>
                                    <td>{loja.descricao}</td>
                                    <td>{loja.cep}</td>                                    
                                    <td><button className="btn btn-danger"  style={{cursor:'pointer'}} onClick={(event)=>this.deletar(loja)}>Deletar</button></td>
                                </tr> 
                            ))}                   
                            </tbody>
                        </table>
                </div>
             </div>
        );
    }
}

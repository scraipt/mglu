import * as React from 'react';

export default class CadProd extends React.Component {
    state = {
        error: null,
        isLoaded: false,       
        produtos:[],
        enderecos:[]
     
    };

    setStateAsync(state) {
		return new Promise((resolve) => {
			this.setState(state, resolve)
		});
    }

    pesquisar = async (event) =>  {
        const cepCliente =  this.state.cepCliente.value;
        const pesquisa = this.state.pesquisa.value;      
       
        this.setState({isLoading: true});
        const myHeaders = new Headers(); myHeaders.append("Content-Type", "application/json");
        const config = { headers:myHeaders,method:'POST',body:JSON.stringify({cepCliente,pesquisa}) }
                            
        const response = await fetch("http://localhost:8080/pesquisar",config);
        const produtos = await response.json();  
        
        if(produtos.error) this.setState({ produtos: produtos.error, isLoading: false});
        await this.setStateAsync({ produtos })
        this.setState({ isLoading: false }) 
    }


    render() {
        const { error, isLoading, produtos} = this.state;                     
         if ((produtos.length) > 0) {
             return (
                <div className="container col-12">
                <div className="row" style={{backgroundColor: "#0083ca"}}>
                    <div  className="col-3" >
                        <img style={{height:'80px'}}src="http://eletrolar.com/wp-content/uploads/2016/04/magazine-luiza-logo.jpg" alt=""/>
                    </div>           
                    <div className="col-9" style={{backgroundColor: "#0083ca",marginTop:'30px'}}>                    
                        <form>
                            <input style={{width:'200px'}} type="text"   ref={(input) => {if (input) this.state.cepCliente = input;  }}   placeholder="cep ou posição geográfica"/>
                            <input style={{width:'500px',marginLeft:'10px'}} type="text"  ref={(input) => {if (input) this.state.pesquisa = input;  }}   placeholder="procure por código ou nome"/>        
                            <button type="button" onClick={(event)=>this.pesquisar(event)}>Pesquisar</button>
                        </form>
                    </div>
                </div>
                
                        <h1>Oba!Você encontrará esse produto nas nossas lojas pertinho de você!</h1>
                        <table className="table table-hover">
                            <thead>
                                <tr>
                                    <th>Descricao</th>
                                    <th>Cód Loja</th>
                                    <th>Preço</th>   
                                    <th>Distância</th>                      
                                </tr>
                            </thead>
                            <tbody>
                            {produtos.map(produto => (
                                <tr key={produto.id}>    
                                    <td>{produto.descricao}</td>                                                                                         
                                    <td>{produto.codFilial}</td>                                   
                                    <td>{produto.valor}</td>
                                    <td>{produto.distancia}</td>                                   
                                </tr> 
                                ))}              
                            </tbody>
                        </table>  
         </div>
             )
         }
        return(
            <div className="container col-12">
                <div className="row" style={{backgroundColor: "#0083ca"}}>
                    <div  className="col-3" >
                        <img style={{height:'80px'}}src="http://eletrolar.com/wp-content/uploads/2016/04/magazine-luiza-logo.jpg" alt=""/>
                    </div>           
                    <div className="col-9" style={{backgroundColor: "#0083ca",marginTop:'30px'}}>                    
                        <form>
                            <input style={{width:'200px'}} type="text"   ref={(input) => {if (input) this.state.cepCliente = input;  }}   placeholder="cep ou posição geográfica"/>
                            <input style={{width:'500px',marginLeft:'10px'}} type="text"  ref={(input) => {if (input) this.state.pesquisa = input;  }}   placeholder="procure por código ou nome"/>        
                            <button type="button" onClick={(event)=>this.pesquisar(event)}>Pesquisar</button>
                        </form>
                    </div>
                </div>         
            </div>
        )
    }
}
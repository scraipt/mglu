import React from 'react';

const Topo = (props) => {
    return (       
        <nav className="navbar navbar-toggleable-md navbar-light bg-faded">		
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item active">
                        <a className="nav-link" href="/">Home</a>
                    </li>	
                    <li className="nav-item active">
                        <a className="nav-link" href="/cadastro-loja">Cadastro de Lojas</a>
                    </li>	
                    <li className="nav-item active">
                        <a className="nav-link" href="/cadastro-produto">Cadastro Produtos</a>
                    </li>	
                    <li className="nav-item active">
                        <a className="nav-link" href="/vendas">Tela de Produtos</a>
                    </li>				
                </ul>
            </div>
        </nav>          
    );
};

export default Topo;
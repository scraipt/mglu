import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import CadLoja from './ui/CadLoja';
import CadProd from './ui/CadProd';
import Vendas from './ui/Vendas';
import registerServiceWorker from './registerServiceWorker';
import {Route,BrowserRouter} from 'react-router-dom';


ReactDOM.render(
    <BrowserRouter >    
        <div>
            <Route path='/' component={App}/>  
            <Route path='/cadastro-loja' component={CadLoja}/>  
            <Route path='/cadastro-produto' component={CadProd}/>  
            <Route path='/vendas' component={Vendas}/>  
        </div>
    </BrowserRouter>,
    document.getElementById('root')
    );
registerServiceWorker();

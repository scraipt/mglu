it('fails, as expected', function(done) { // <= Pass in done callback
    request('http://localhost:8080')
    .get('/')
    .end(function(err, res) {
      expect(res).to.have.status(123);
      done();                               // <= Call done to signal callback end
    });
  }) ;
  

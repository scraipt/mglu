var http       = require('http');
var mysql      = require('mysql');
var express    = require('express');
var request    = require('request');
var bodyParser = require('body-parser');
var app        = express();
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods","POST, GET, PUT, DELETE");
	res.header("Content-Type","application/json");
	next();
});

connection = mysql.createConnection({host:'localhost',user:'root',password:'root',database:'testmglu'});


app.get('/', function (req, res) {
	res.send("bem vindo a api de test/dev-mglu");
});

//nesse método eu pesquiso o produto que o cliente está procurando.
app.post('/pesquisar', function (req, res) {
	//1 - PASSO identificar o meu produto para que eu consiga encontrar os ceps "proximos a ele"
	const cepCliente =  req.body.cepCliente;
	const pesquisa =  req.body.pesquisa;
	var retorno=[];
	//trago os meus produtos	
	const query = "select  p.codproduto, p.vlrVendas, f.codFilial, f.descricao, f.cep from produtos  p inner join lojas f  on p.codFilial = f.codFilial where p.codproduto like " + connection.escape('%'+pesquisa+'%') + " or p.descricao like " + connection.escape('%'+pesquisa+'%');

	function getCeps(){
		return new Promise(fn);
	  
		function fn(resolve,reject){
			connection.query(query, function (error, results, fields) {
				if (error) throw error;
				return resolve(results);
			});	
		  }
	} 

  getCeps().then(function(retorno){
	var cepsLojas = [];
	for (var i = 0;i < (retorno.length); i++) {	
		cepsLojas = cepsLojas + retorno[i]['cep'] + "|";
		
	}  
	let cepsFmt;
	cepsFmt = cepsLojas.substr(0,(cepsLojas.length - 1));

	let arrRetorno = [];
	function getDistancias(){
		return new Promise(fn);
		function fn(resolve,reject){
		let url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins="+cepsFmt+"&destinations="+cepCliente+"&mode=driving&language=pt-BR&sensor=false"
			request.get(url, function (err, response, body) {
				returnCeps = JSON.parse(response.body);	
				
				for (var i = 0;i < (retorno.length); i++) {
					//console.log(returnCeps.rows[i]);
					if (returnCeps.rows[i].elements[0].status == "OK") {						
						var obj = {
							"id":i,
							"cep":retorno[i]['cep'],
							"descricao":retorno[i]['descricao'],
							"codFilial":retorno[i]['codFilial'],
							"valor":retorno[i]['vlrVendas'],
							"distancia":returnCeps.rows[i].elements[0]['distance'].text,
							"status":"ok"
								};				  			
						arrRetorno.push(obj);
						//return resolve(arrRetorno);				
					} else {
						var obj = {
						"id":i,
						"cep":"",
						"descricao":"",
						"codFilial":"",
						"valor":"",
						"distancia":"",
						"status":"erro"
							};				  			
					arrRetorno.push(obj);
					}
				}				
				return resolve(arrRetorno);	
			});  				
		}
	}
	getDistancias().then(function(arrRetorno){
		res.send(arrRetorno);
	});
});
	
});


/* LOJAS */
app.get('/lojas', function (req, res) {
	connection.query("select * from lojas", function (error, response, body) {
		if (error) throw error;
		res.send(response);
	});	
});

app.post('/lojas', function (req, res) {

		const codFilial  = req.body.codFilial;
		const descricao = req.body.descricao;
		const cep 	 = req.body.cep;
		
		connection.query("insert into lojas (codFilial,descricao,cep) VALUES (?,?,?)",[codFilial,descricao,cep], function (error, response, body) {
        	if (error) throw error;
        	res.send(response);
		});	
});

app.put('/lojas', function (req, res) {

	const codFilial  = req.body.codFilial;
	const descricao = req.body.descricao;
	const cep 	 = req.body.cep;
	
	connection.query("update lojas set descricao = ?, cep = ?",[codFilial,descricao,cep], function (error, response, body) {
		if (error) throw error;
		res.send(response);
	});	
});

app.delete('/lojas', function (req, res) {
	const codFilial  = parseInt(req.body.codFilial,10);
	connection.query("delete from lojas where codFilial = ? ",[codFilial], function (error, response, body) {
		if (error) throw error;
		res.send(response);
	});
});

/* END LOJAS */


/* PRODUTOS */
app.get('/produtos', function (req, res) {
	connection.query("select * from produtos order by codFilial", function (error, response, body) {
		if (error) throw error;
		res.send(response);
	});	

});

app.post('/produtos', function (req, res) {
		var codFilial  = req.body.codFilial;
		var codProd    = req.body.codigoproduto;
		var descricao  = req.body.descricao;
		var vlrVenda   = req.body.preco;
		
	connection.query("insert into produtos (codFilial,codproduto,descricao,vlrVendas) VALUES (?,?,?,?)",[codFilial,codProd,descricao,vlrVenda], function (error, response, body) {
		if (error) throw error;
		res.send(response);
	});

});

app.delete('/produtos', function (req, res) {
	var codFilial  = req.body.codFilial;
	var codproduto    = req.body.codproduto;
	
	connection.query("delete from produtos where codFilial = ? and codproduto = ?",[codFilial,codproduto], function (error, response, body) {
		if (error) throw error;
		res.send(response);
	});

});


let port = process.env.PORT || 8080;
let server = app.listen(port, () => {
  console.log('App listening on port %s', port);
});

module.exports = server;

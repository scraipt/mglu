-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 29-Jan-2018 às 17:05
-- Versão do servidor: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testmglu`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `lojas`
--

CREATE TABLE `lojas` (
  `codFilial` int(11) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `cep` varchar(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `lojas`
--

INSERT INTO `lojas` (`codFilial`, `descricao`, `cep`) VALUES
(1, 'MATRIZ - FRANCA/SP', '14403638'),
(3, 'LOJA RIBEIRÃO PRETO', '14010060'),
(16, 'LOJA RIBEIRÃO PRETO', '14010000'),
(54, 'LOJA RIBEIRAO PRETO', '14055630');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `codFilial` int(11) NOT NULL,
  `codproduto` int(11) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `vlrVendas` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`codFilial`, `codproduto`, `descricao`, `vlrVendas`) VALUES
(1, 1555363, 'Smartphone Motorola Moto G5s Plus 32GB', 1208),
(3, 1934101, 'Smart TV LED 40” Samsung 4K/Ultra HD 40MU6100 ', 1035),
(3, 2186702, 'Guarda-roupa Casal 3 Portas Madesa', 2089),
(54, 1555363, 'Smartphone Motorola Moto G5s Plus 32GB', 1035),
(54, 1934101, 'Smart TV LED 40” Samsung 4K/Ultra HD 40MU6100 ', 3.048);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lojas`
--
ALTER TABLE `lojas`
  ADD PRIMARY KEY (`codFilial`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`codFilial`,`codproduto`);

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `produtos`
--
ALTER TABLE `produtos`
  ADD CONSTRAINT `FK_FILIAL` FOREIGN KEY (`codFilial`) REFERENCES `lojas` (`codFilial`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
